# Gnome3 Xinput Layout Switch

Switch Gnome3 keyboard layout on release of two keys combination  (Ctrl Shift by default).
It is aimed to workaround https://bugs.launchpad.net/ubuntu/+source/gnome-control-center/+bug/36812

It uses *xinput* under the hood and runs gdbus call each time to switch between tho most recent layouts in gnome with respect to status bar indication


## Configuration
Has command line arguments
* *--debug* - dump all keyboard events to show key codes
* *--key1* - first key code to monitor (default: 37,105 [Ctrl])
* *--key2* - second key code to monitor (default: 50,62 [Shift])

## Setup instructions:

The simple way to set it up with the default configuration by running following commands in terminal:

    Install [xkb-switch](https://github.com/grwlf/xkb-switch)

    $ git clone https://gitlab.com/sankam/xfce4_xinput_layout_switch.git

    $ cd xfce4_xinput_layout_switch

    $ go generate

Instead of build it from source yuu can download it from [release page](https://gitlab.com/softkot/gnome3_xinput_layout_switch/-/releases) and continue afterward.

    $ sudo cp xinput-layout-switch /usr/bin/xinput-layout-switch

    add xinput-layout-switch to autoload

Then remove or disable gnome builtin keyboard shortcuts and restart X11.


## Compilation

```bash
docker run --rm -it -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang:1.14 bash
go build -o ./xinput-layout-switch -ldflags "-s -w"
```


P.S.

* In case you want to change layout switch to Alt + Shift pass *--key1 64,108* argument.

* In case you want to change layout switch to Ctrl + Alt pass *--key2 64,108* argument.
